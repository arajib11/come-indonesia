<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Come_Indonesia
 */

?>
</div><!-- #content -->


  <!-- Footer -->
  <footer class="w-full flex flex-wrap overflow-hidden text-white justify-center bottom-0 ">
      <div
        class="wrap-nav-bottom flex flex-wrap w-full md:mx-auto -m-2 px-6 lg:px-16 pt-8 container"
      >
        <div
          class="info-nav-bottom my-2 px-2 w-full overflow-hidden md:w-full lg:w-2/5 xl:w-1/4"
        >
          <div class="description lg:w-64  text-white">
            <p class=" text-white">
              <b>COME</b> adalah aplikasi yang mempertemukan traveller dan
              traveller lainnya dengan pemilik paket wisata yang membutukan
              minimum peserta untuk berangkat.
            </p>
            <p class="address mt-8 w-full text-white">
              <b>Jl. Jend Sudirman Kav 52-53 SCBD Lot.8</b> <br />
              Jakarta Selatan <br />
              12190
            </p>
          </div>
        </div>
        <div
          class="nav-footer my-2 px-2 w-full overflow-hidden md:w-full lg:w-3/5 xl:w-3/4 "
        >
          <div class="list flex flex-wrap -mx-2 overflow-hidden ">
            <ul class="list-none px-2 w-1/2 lg:w-1/4 overflow-hidden ">
              <h2 class="font-semibold mt-4 md:mt-0 mt-4 md:mt-0">Come App</h2>
              <li><a href="https://comeapp.id/features">Feature</a></li>
              <li><a href="https://comeapp.id/download">Download</a></li>
              <li><a href="https://comeapp.id/howto">How Come Works</a></li>
              <li><a href="https://comeapp.id/terms">Terms and Policy </a></li>
            </ul>
            <ul class="list-none px-2 w-1/2 lg:w-1/4 overflow-hidden">
              <h2 class="font-semibold mt-4 md:mt-0">Company</h2>
              <li><a href="">About</a></li>
              <li><a href="">Careers</a></li>
              <li><a href="">Brand</a></li>
              <li><a href="https://partner.comeapp.id/">Partner</a></li>
            </ul>
            <ul class="list-none px-2 w-1/2 lg:w-1/4 overflow-hidden">
              <h2 class="font-semibold mt-4 md:mt-0">Download</h2>
              <li><a href="https://play.google.com/store/apps/details?id=id.comeapp.come">Android</a></li>
              <li><a href="https://itunes.apple.com/id/app/come/id1405693017?mt=8">iOS</a></li>
            </ul>
            <ul class="list-none px-2 w-1/2 lg:w-1/4 overflow-hidden">
              <h2 class="font-semibold mt-4 md:mt-0">Help</h2>
              <li><a href="https://comeapp.id/faq">FAQ</a></li>
              <li><a href="https://comeapp.id/refund_policy">Refund Policy</a></li>
              <li><a href="https://www.instagram.com/come_id">Instagram</a></li>
              <li><a href="https://twitter.com/comeappid">Twitter</a></li>
              <li><a href="https://www.facebook.com/comeindonesia">Facebook</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="w-full flex justify-center items-center my-8 px-4">
        <div
          class="copyrigh mx-auto flex justify-center flex-wrap w-full text-center"
        >
          <h5 class="font-semibold text-xs w-full md:text-base">
            Copyright © 2019 PT. GLOBAL TEKNOKREASI INDONESIA All Rights
            Reserved
          </h5>
        </div>
      </div>
    </footer>
    <!-- End Footer -->
</div><!-- #page -->



<?php wp_footer(); ?>

</body>
</html>
