<?php get_header(  ); ?>
<div class="container mx-auto">

    <div class="content container pt-10 px-4 antialiased  flex flex-wrap overflow-hidden ">
        <div class="w-full markdown bantuan">
            <!-- <div class="meta-info font-light text-sm mt-4 mb-2">
                <h4 class="font-light m-0 "><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></h4>
            </div> -->
            <div class="font-bold text-gray-700 leading-tight mb-4 text-3xl text-center">
                <h1 class="mt-0 "><?php the_title();?></h1>
            </div>
            <div class="post leading-relaxed xl:text-base w-full mb-20">
                <div class="wrapper-bantuan lg:mx-32">
                <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post() ; ?>
                    <?php the_content(); ?>                     
                    <?php 
                        endwhile;

                    else :
                        echo "<p> No Content Found</p>";

                    endif;?>
                </div>
            </div>
            <div class="hubungi-kami flex w-full justify-center items-center flex-wrap flex-col mb-20">
                <div class="w-full flex justify-center">
                    <p class=" text-xl md:text-2xl text-blue-400 text-center">Tidak menemukan jawaban?, Dapatkan bantuan yang lebih personal</p>
                </div>
                <div class="w-full  flex justify-center">
                    <a href="https://comeapp.id/contact"><button class="w-32 flex items-center h-12 rounded-lg bg-blue-400 text-white text-center justify-center font-semibold hover:bg-blue-500">Hubungi Kami</button></a>
                </div>

            </div>

        </div>

    </div>
</div>
 
    <?php get_footer(  ); ?>