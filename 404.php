<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Come_Indonesia
 */

get_header();
?>

	<div id="primary" class="content-area mb-24 ">
		<main id="main" class="site-main flex container mx-auto justify-center items-center">

			<section class="error-404 not-found container mx-auto justify-center flex-wrap mt-24">
				<header class="page-header flex justify-center my-12 flex-wrap border-b w-3/6 mx-auto">
					<h1 class="page-title font-extrabold text-2xl lg:text-4xl text-blue-400 mb-12"><?php esc_html_e( 'Oops!', 'come-indonesia' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content flex items-center flex-col justify-center flex-wrap container mx-auto">
				
					<p class=" text-xl font-light mb-12 text-center "><?php esc_html_e( "We can't seem to find the page you're looking for.", 'come-indonesia' ); ?></p>

					<a href="https://comeapp.id/"><button class="w-42 py-3 px-6 flex items-center h-14 rounded-lg bg-blue-400 text-white text-center font-semibold hover:bg-blue-500">Back to homepage.</button></a>
				</div><!-- .page-content -->

				
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
