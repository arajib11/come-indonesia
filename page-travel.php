<?php get_header(  ); ?>
<div class="container mx-auto">
    <div class="landing-img clearfix md:pt-2">
        <!-- <img src="http://localhost/wordpress/wp-content/uploads/2020/01/College-Friends_LandingPage_1440x700.jpg" alt="" class=""> -->
        <?php 
           if (class_exists('MultiPostThumbnails')) : 
            MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image', NULL,  'secondary-featured-thumbnail');
            endif;
        ?>
    </div>

    <div class="content container pt-10 px-4 antialiased  flex flex-wrap overflow-hidden md:-mx-2">
        <div class="hidden md:flex md:w-1/6 md:px-2 xl:w-2/6 justify-end xl:justify-center">
            <a class="text-gray-600 text-4xl md:pr-4 lg:pr-8 font-light h-20 " href="<?php echo home_url(); ?>"><i class="fal fa-chevron-circle-left"></i></a>
        </div>
        <div class="md:w-5/6 md:px-2 w-full xl:w-4/6  markdown">
            <div class="meta-info font-light text-sm mt-4 mb-2">
                <h4 class="font-light m-0 "><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></h4>
            </div>
            <div class="title font-extrabold text-gray-700 leading-tight mb-4 md:mb-10 text-xl">
                <h1 class="mt-0"><?php the_title();?></h1>
            </div>
            <div class="post leading-relaxed xl:text-base w-full">
            <?php
							if (have_posts()) :
								while (have_posts()) : the_post() ; ?>
            <?php the_content(); ?>                     
            <?php 
								endwhile;

							else :
								echo "<p> No Content Found</p>";

							endif;?>

            </div>

        </div>

    </div>
</div>
 
    <?php get_footer(  ); ?>