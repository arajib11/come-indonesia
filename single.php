<?php get_header(  ); ?>

    <div class="landing-img clearfix md:pt-2 w-full">
        <!-- <img src="http://localhost/wordpress/wp-content/uploads/2020/01/College-Friends_LandingPage_1440x700.jpg" alt="" class=""> -->
        <?php 
           if (class_exists('MultiPostThumbnails')) : 
            MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image', NULL,  'secondary-featured-thumbnail');
            endif;
        ?>
    </div>
    <div class="container mx-auto">
    <div class="content container pt-10 px-4 antialiased flex flex-wrap overflow-hidden md:-mx-2">
        <div class="hidden md:flex md:w-2/12 xl:w-1/4 md:px-2 justify-center items-start">
            <a class="text-black hover:text-blue-600 text-4xl font-light  mt-2 lg:ml-6 text-gray-700 " href="<?php echo home_url(); ?>"><i class="fal fa-chevron-circle-left"></i></a>
        </div>
        <div class="md:w-10/12 xl:w-3/4 lg:pl-8 xl:pr-32 md:pr-12 w-full markdown">
            <div class="meta-info font-light text-sm mt-4 mb-2">
                <h4 class="font-normal m-0 text-gray-500 "><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></h4>
            </div>
            <div class="title font-bold mb-4 tracking-tight leading-none">
                <h1 class="mt-0 text-4xl font-bold text-gray-700"><?php the_title();?></h1>
            </div>
            <div class="post leading-relaxed xl:text-base w-full">
            <?php
							if (have_posts()) :
								while (have_posts()) : the_post() ; ?>
            <?php the_content(); ?>                     
            <?php 
								endwhile;

							else :
								echo "<p> No Content Found</p>";

							endif;?>

            </div>

        </div>

    </div>
</div>
 
    <?php get_footer(  ); ?>