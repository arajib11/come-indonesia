/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
(function() {
    var container, button, menu, links, i, len;

    container = document.getElementById("site-navigation");
    if (!container) {
        return;
    }

    button = container.getElementsByTagName("button")[0];
    if ("undefined" === typeof button) {
        return;
    }

    menu = container.getElementsByTagName("ul")[0];

    // Hide menu toggle button if menu is empty and return early.
    if ("undefined" === typeof menu) {
        button.style.display = "none";
        return;
    }

    menu.setAttribute("aria-expanded", "false");
    if (-1 === menu.className.indexOf("nav-menu")) {
        menu.className += " nav-menu";
    }

    button.onclick = function() {
        if (-1 !== container.className.indexOf("toggled")) {
            container.className = container.className.replace(" toggled", "");
            button.setAttribute("aria-expanded", "false");
            menu.setAttribute("aria-expanded", "false");
        } else {
            container.className += " toggled";
            button.setAttribute("aria-expanded", "true");
            menu.setAttribute("aria-expanded", "true");
        }
    };

    // Get all the link elements within the menu.
    links = menu.getElementsByTagName("a");

    // Each time a menu link is focused or blurred, toggle focus.
    for (i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener("focus", toggleFocus, true);
        links[i].addEventListener("blur", toggleFocus, true);
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
        var self = this;

        // Move up through the ancestors of the current link until we hit .nav-menu.
        while (-1 === self.className.indexOf("nav-menu")) {
            // On li elements toggle the class .focus.
            if ("li" === self.tagName.toLowerCase()) {
                if (-1 !== self.className.indexOf("focus")) {
                    self.className = self.className.replace(" focus", "");
                } else {
                    self.className += " focus";
                }
            }

            self = self.parentElement;
        }
    }

    /**
     * Toggles `focus` class to allow submenu access on tablets.
     */
    (function(container) {
        var touchStartFn,
            i,
            parentLink = container.querySelectorAll(
                ".menu-item-has-children > a, .page_item_has_children > a"
            );

        if ("ontouchstart" in window) {
            touchStartFn = function(e) {
                var menuItem = this.parentNode,
                    i;

                if (!menuItem.classList.contains("focus")) {
                    e.preventDefault();
                    for (i = 0; i < menuItem.parentNode.children.length; ++i) {
                        if (menuItem === menuItem.parentNode.children[i]) {
                            continue;
                        }
                        menuItem.parentNode.children[i].classList.remove("focus");
                    }
                    menuItem.classList.add("focus");
                } else {
                    menuItem.classList.remove("focus");
                }
            };

            for (i = 0; i < parentLink.length; ++i) {
                parentLink[i].addEventListener("touchstart", touchStartFn, false);
            }
        }
    })(container);
})();

// (function($) {
//     $("div.grabnav-tab-menu>div.list-group>a").click(function(e) {
//         e.preventDefault();
//         var bg = $(this).attr("data-bg");
//         $(".menu-wrap  .dropdown-menu").css(
//             "background-image",
//             "linear-gradient(to right, rgba(246,248,250,1) 40%, rgba(246,248,250,0.5)), url(" +
//             bg +
//             ")"
//         );

//         $(this)
//             .siblings("a.active")
//             .removeClass("active");
//         $(this).addClass("active");
//         var index = $(this).index();
//         $("div.grabnav-tab>div.grabnav-tab-content").removeClass("active");
//         $("div.grabnav-tab>div.grabnav-tab-content")
//             .eq(index)
//             .addClass("active");
//     });
// })(jQuery);

// Look for .hamburger
var hamburger = document.querySelector(".hamburger");
var mainDropmenu = document.querySelector(".main-dropdown");
// On click
hamburger.addEventListener("click", function() {
    // Toggle class "is-active"
    hamburger.classList.toggle("is-active");
    mainDropmenu.classList.toggle("active");

    // Do something else, like open/close menu
});

mainDropmenu.addEventListener("blur", function() {
    mainDropmenu.removeClass("active");
});

// Look for .hamburger
var hamburgerDesktop = document.querySelector(".hamburger-desktop");
var mainDropmenuDesktop = document.querySelector(".main-dropdown-desktop");
// On click
hamburgerDesktop.addEventListener("click", function() {
    // Toggle class "is-active"
    hamburgerDesktop.classList.toggle("is-active");
    mainDropmenuDesktop.classList.toggle("active-menu");

    // Do something else, like open/close menu
});

mainDropmenu.addEventListener("blur", function() {
    mainDropmenu.removeClass("active-menu");
});


$(document).ready(function() {
    var $searchTrigger = $('[data-ic-class="search-trigger"]'),
        $searchInput = $('[data-ic-class="search-input"]'),
        $searchClear = $('[data-ic-class="search-clear"]');

    $searchTrigger.click(function() {
        // var $this = $('[data-ic-class="search-trigger"]');
        $searchTrigger.addClass("active-search");
        $searchInput.focus();
    });

    $searchInput.blur(function() {
        if ($searchInput.val().length > 0) {
            $searchInput.focus();
            return false;
        } else {
            $searchTrigger.removeClass("active-search");
        }
    });


    $searchInput.focus(function() {
        $searchTrigger.addClass("active-search");
    });
});



// Swiper 
var swiper = new Swiper(".swiper-container", {
    slidesPerView: "auto",
    spaceBetween: 5,
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false
    },
    pagination: {
        el: ".swiper-pagination",
        type: 'bullets',
        clickable: true
    },

    breakpoints: {
        640: {
            slidesPerView: 2,
            spaceBetween: 30
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            }
        },
        1024: {
            slidesPerView: 1,
            spaceBetween: 50
        }
    }
});