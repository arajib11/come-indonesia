<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Come_Indonesia
 */

get_header();
?>

<div class="line-come w-full h-10 bg-blue-400 flex">
  <div class="flex items-center container mx-auto">
    <!-- <h1 class="text-xl text-white md:ml-16 ml-10 font-normal ">Come Blog</h1> -->
  </div>
</div>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <!-- Swiper Mobile -->

    <div class="container-card  flex justify-center items-start flex-wrap">
      <div class="swiper-container">
        <div class="swiper-wrapper bg-gray-100">
          <?php 
            // the query
            


            $slidePost = new WP_Query(array(
              'posts_per_page' => 6,
              'category_name' => 'slide-post', // this is the category SLUG
          )); ?>

          <?php if ( $slidePost->have_posts() ) : ?>

          <!-- the loop -->
          <?php while ( $slidePost->have_posts() ) : $slidePost->the_post(); ?>

          <div
            class="swiper-slide card bg-white py-4 rounded-lg md:rounded-none md:flex md:flex-row md:justify-center mb-12 ">
            <div class="content-slider w-full md:flex  md:flex-wrap md:overflow-hidden md:-mx-2 md:items-start justify-center">
              <div class="wp-img md:w-3/5  md:my-2 md:px-2 bg-cover relative max-w-5xl ">
                <?php 
                      if (class_exists('MultiPostThumbnails')) : 
                        MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image', NULL,  'landing-thumbnail');
                        endif;
                    ?>
              </div>
              <div
                class="wp-content px-4 py-5 md:px-2  md:w-2/5  md:pl-12 lg:pr-24 md:pr-16 flex flex-col w-full h-full md:items-center max-w-2xl justify-start">
                <h1 class="font-bold text-gray-700 text-xl md:text-2xl leading-none w-full m-0 xl:mt-6">
                  <?php the_title();?>
                </h1>
                <div class=" float-left w-full text-sm pt-3 font-bold md:text-base md:leading-none text-gray-500 lg:mb-5 ">
                  <?php
                    $posttags = get_the_tags();
                    if ($posttags) {
                      foreach($posttags as $tag) {
                        echo "<h2> Blog,  $tag->name</h2>";
                      }
                    }
                  ?>
                </div>
                <div class=" paraf-post text-xs md:text-xl md:mt-2 w-full">
                  <?php the_excerpt(); ?>
                </div>
                <div class="order-4 flex md:items-start justify-end w-full h-full items-end md:px-4 md:mt-8">
                  <button
                    class="read-more md:w-20 md:h-8 lg:w-24 lg:h-10 items-center flex justify-center text-sm md:shadow-md md:hover:shadow-lg mt-4">
                    <a class=" md:text-base" href="<?php the_permalink(); ?>">Read More</a>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <!-- </div> end slide -->


          <?php endwhile; ?>
          <!-- end of the loop -->

          <!-- pagination here -->

         

          <?php else : ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>

          
        </div>
        <!-- <?php wp_reset_postdata(); ?> -->
        <!-- End-wrapper -->
       
        <div class="hidden md:block">
          <div class="swiper-button-next hidden md:block"></div>
          <div class="swiper-button-prev hidden md:block"></div>
        </div>
        <div class="swiper-pagination"></div>
      </div>
    </div>
    <!-- Index -->
    <section class="article-hero md:mt-24 flex flex-wrap justify-center pb-10 flex-col mx-auto container ">
      <div class="top-heading mb-8">
        <p class="font-bold text-3xl mt-20 md:mt-0">Articles</p>
        <form role="search" method="get" class="search-form mb-8 " action="<?php echo home_url( '/' ); ?>">
          <div class="search-article flex items-center px-4 py-2">
            <label>
              <input type="search" class="search-field search-text w-48 md:w-72"
                placeholder="<?php echo esc_attr_x( 'Search articles by keyword', 'placeholder' ) ?>"
                value="<?php echo get_search_query() ?>" name="s"
                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
            </label>
            <button type="submit" class="search-submit"><i
                class="fa fa-search cursor-pointer ml-2 text-sm"></i></button>
            <!-- <i class="fa fa-search search-submit cursor-pointer text-black ml-2 text-sm"></i> -->
          </div>
        </form>
        <div class="filter-category flex-wrap flex-row my-2">
          <?php wp_nav_menu( array( 'theme_location' => 'filter' ) ); ?>
        </div>
      </div>

      <!-- Posts -->
      <div class="container-post w-full flex flex-wrap justify-center items-center  ">
        <div class="wrapper-posts flex w-full justify-center items-center flex-wrap container mx-auto ">
          <?php
							if (have_posts()) :
								while (have_posts()) : the_post() ; ?>
          <div class="card-posts bg-white rounded relative shadow-sm pb-5">
            <div class="img-thumbnail w-full">
               <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( ) ?></a>
            </div>
            <div class="text-post px-6">
              <div class="font-bold text-sm text-blue-400 mt-4 mb-2">
                <?php
                    $posttags = get_the_tags();
                    if ($posttags) {
                      foreach($posttags as $tag) {
                        echo "<h2> Blog,  $tag->name</h2>";
                      }
                    }
                  ?>
              </div>
              <h1 class="leading-none font-bold text-xl  mb-2">
                <a class="text-black hover:underline" href="<?php the_permalink(); ?>"><?php the_title();?></a>
              </h1>
              <div class="leading-tight">
                <?php the_excerpt(); ?>
              </div>
              <a href="<?php the_permalink(); ?>" class="float-right hover:underline md:mt-4 text-blue-400 ">Read More ></a>
            </div>
          </div>
          <?php 

            endwhile;

            else :
            echo "<p> No Content Found</p>";

            endif;?>
        </div>
      </div>
      <div class="pagination h-24">
        <?php $args = array(
            'prev_text'          => __('<i class="fa fa-chevron-left"></i>'),
            'next_text'          => __('<i class="fa fa-chevron-right"></i>'),
           
              ); ?>
        <?php echo paginate_links($args); ?>
      </div>
    </section>

    <!-- End Posts -->

  </main><!-- #main -->
</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();