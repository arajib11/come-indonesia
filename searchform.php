<div class="div bg-transparent w-8 h-8 absolute z-30" data-ic-class="search-trigger"></div>
<form role="search" method="get" class="search-form flex items-center float-right rounded-full px-2 py-2 h-8"
data-ic-class="search-trigger" action="<?php echo home_url( '/' ); ?>">
    <label>
        <input type="search" class="search-field search-box search-input search-box bg-transparent outline-none border-none pr-2" data-ic-class="search-input"
            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </label>
    <button type="input" class="search-submit text-gray-700 text-xl"><i class="fa fa-search cursor-pointer"></i></button>
</form>
