<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Come_Indonesia
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
 
  <?php wp_head(); ?>
       <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src=“https://www.googletagmanager.com/gtag/js?id=UA-154468546-1”></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag(‘js’, new Date());
    gtag(‘config’, ‘UA-154468546-1’);
  </script>
</head>

<body <?php body_class( ); ?>>
  <div id="page" class="site">
    <!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'come-indonesia' ); ?></a> -->
    <!-- Header -->

    <header id="masthead" class="w-full fixed bg-white  flex h-16 z-30 md:h-20 border-b border-black-200">
      <div id="nav-dekstop"
        class="flex w-full px-4 lg:px-12 mx-auto items-center justify-between flex-wrap hidden md:flex container">
        <div class=" lg:w-1/4 w-1/2 md:w-1/3 flex items-center">
          <div class="main-menu">
            <button class="hamburger-desktop hamburger--collapse focus:outline-none" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
            <div id="dekstop-menu"
              class="dropdown-menu main-dropdown-desktop markdown absolute hidden bg-gray-100 w-28 mt-3">
              <div class="grabnav-tab-container ">
                <div class="grabnav-tab-menu">
                  <div class="list-group">
                    <a href="https://comeapp.id/" class="list-group-item text-center text-gray-700 font-semibold text-lg">Tentang Come</a>
                    <a href="#" class="list-group-item text-center text-gray-700 font-semibold text-lg">Pengguna</a>
                    <a href="#" class="list-group-item text-center text-gray-700 font-semibold text-lg">Travel Agen</a>
                    <a href="https://www.linkedin.com/company/comeapp-id/" class="list-group-item text-center text-gray-700 font-semibold text-lg">Perusahaan</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="logo md:ml-6 cursor-pointer">
            <a href="<?php echo home_url(); ?>"><img class="mt-2 pb-4 h-12 md:h-16 md:ml-4"
                src="<?php bloginfo( 'template_url' ); ?>/img/Comeicon_logo.png" alt="" /></a>
          </div>
        </div>
        <div class="search flex items-center w-1/2 lg:w-3/4 md:w-2/3 justify-end items-baseline">
          <!-- secondary-menu -->
          <nav id="secondary-menu" class="w-full flex justify-end float-right hidden md:flex flex-wrap">
            <ul class=" flex p-0">
              <li class="list-none relative pr-6">
                <!-- <input type="checkbox" class="toggler-info"  /> -->
                <a class=" text-base lg:text-lg font-semibold  text-gray-700"
                  href="https://blog.comeapp.id/cara-lengkap-menjadi-mitra-come/">Jadilah Mitra Kami</a>
              </li>
              <li class="list-none relative pr-6"><a class="text-base lg:text-lg font-semibold text-gray-700"
                  href="https://comeapp.id/contact">Hubungi Kami</a>
              </li>
              <li class="list-none relative lg:pr-6"><a class="  text-base lg:text-lg font-semibold text-gray-700" href=""><i
                    class="fal fa-globe "></i> Bahasa Indonesia</a></li>
            </ul>
          </nav>
          <!-- end secondary menu -->
          <?php get_search_form( ); ?>
        </div>
      </div>


      <!-- Nav Mobile -->
      <div id="nav-mobile" class="flex w-full px-4 lg:px-12 mx-auto items-center justify-between flex-wrap md:hidden">
        <div class=" lg:w-1/4 w-1/2 md:w-1/3 flex items-center">
          <div class="main-menu">
            <button class="hamburger hamburger--collapse focus:outline-none" type="button">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
            <div id="mobile-menu"
              class="dropdown-menu main-dropdown absolute md:mt-2  hidden bg-gray-100 w-11/12 overflow-scroll">
              <!-- <?php wp_nav_menu( array( 'theme_location' => 'main_menu_mobile' ) ); ?> -->
              <div class="menu-main-menu-mobile-container">
                <ul id="menu-main-menu-mobile" class="menu">
                  <li id="menu-item-372" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-372">
                    <a href=" https://blog.comeapp.id/cara-lengkap-menjadi-mitra-come/">Jadilah Mitra Kami</a></li>
                  <li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371">
                    <a href=" https://comeapp.id/contact">Hubungi Kami</a></li>
                  <li id="menu-item-373" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-373">
                    <a href="https://comeapp.id/">Tentang Come</a></li>
                  <li id="menu-item-374" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-374">
                    <a href="#">Pengguna</a></li>
                  <li id="menu-item-375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-375">
                    <a href="#">Travel Agen</a></li>
                  <li id="menu-item-375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-375">
                    <a href="#">Perusahaan</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div class="logo md:ml-6 cursor-pointer">
            <a href="<?php echo home_url(); ?>"><img class="mt-2 pb-4 h-12 md:h-16 md:ml-4"
                src="<?php bloginfo( 'template_url' ); ?>/img/Comeicon_logo.png" alt="" /></a>
          </div>
        </div>
        <div class="search flex items-center w-1/2 lg:w-3/4 md:w-2/3 justify-end">
          <?php get_search_form( ); ?>
        </div>
      </div>



    </header>
    <!-- Header MD -->
    <div class="line w-full h-10 md:h-12 flex mb-6 md:mb-2">
      <div class="flex items-center">
        <h1 class="text-xl text-white ml-6 font-bold ">Blank</h1>
      </div>
    </div>


    <div id="content" class="site-content antialiased">